#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define ITEM_DB "item.db"

typedef struct item{
    int id;
    char name[80];
    int price;
    double quantity;
}item_t;
